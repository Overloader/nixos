{
  description = "Overloaders Nixos config";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur.url = "github:nix-community/NUR";

    nix-index-database = {
      url = "github:nix-community/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    grub2-themes = {
      url = "github:vinceliuice/grub2-themes";
    };

    nixvim.url = "github:nix-community/nixvim";
  };

  outputs =
    {
      nixpkgs,
      home-manager,
      nur,
      nix-index-database,
      nixvim,
      ...
    }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;

        config = {
          allowUnfree = true;
        };
      };
    in
    {
      nixosConfigurations =
        let
          modules = device_module: [
            {
              nixpkgs.overlays = [
                nur.overlays.default
              ];
            }
            ./system/configuration.nix
            device_module
            home-manager.nixosModules.home-manager
            {
              home-manager = {
                useGlobalPkgs = true;
                useUserPackages = true;
                users.lukas = import ./home/home.nix;
                extraSpecialArgs = {
                  inherit inputs;
                  inherit nixvim;
                };
              };
            }
            nix-index-database.nixosModules.nix-index
            inputs.grub2-themes.nixosModules.default
          ];
        in
        {
          lnix = nixpkgs.lib.nixosSystem {
            inherit system;
            modules = modules { device.lnix.enable = true; };
          };
          pnix = nixpkgs.lib.nixosSystem {
            inherit system;
            modules = modules { device.pnix.enable = true; };
          };
        };
      homeConfigurations."lukas" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        modules = [ ./home/home.nix ];
        extraSpecialArgs = {
          inherit inputs;
          nixosConfig.device = { };
        };
      };
      devShells."${system}".default = pkgs.mkShell {
        packages = with pkgs; [
          lua-language-server
          stylua
        ];
      };
    };
}

{
  pkgs,
  lib,
  config,
  ...
}:
let
  alpha = "dd";
  selection = "#44475a";

  orange = "#ffb86c";
  red = "#ff5555";
  magenta = "#ff79c6";
  blue = "#6272a4";
  green = "50fa7b";
  pictures = import ./pictures.nix { inherit pkgs lib; };
in
{

  options.swaylock.enable = lib.mkEnableOption "Swaylock Configs";

  config = lib.mkIf config.swaylock.enable {
    home.packages = [
      (pkgs.writeShellScriptBin "random_lock" ''
        ${pictures.random_image}/bin/random_image | xargs swaylock -i
      '')
      pictures.random_image
    ];
    programs.swaylock = {
      enable = true;
      package = pkgs.swaylock-effects;
      settings = {
        scaling = "fill";
        inside-ver-color = "${selection}${alpha}";
        inside-wrong-color = "${selection}${alpha}";
        inside-color = "${selection}${alpha}";
        ring-ver-color = "${green}${alpha}";
        ring-wrong-color = "${red}${alpha}";
        ring-color = "${blue}${alpha}";
        line-uses-ring = true;
        key-hl-color = "${magenta}${alpha}";
        bs-hl-color = "${orange}${alpha}";
        separator-color = "${selection}${alpha}";
        text-ver-color = "${green}";
        text-wrong-color = "${red}";
        time-text-color = "${blue}";
        clock = true;
        indicator = true;
        timestr = "%H:%M:%S";
        datestr = "%A %e %B %Y";
        indicator-radius = 120;
        hide-keyboard-layout = true;
      };
    };
  };
}

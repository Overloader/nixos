{
  config,
  lib,
  pkgs,
  ...
}:
{
  options.firefox.enable = lib.mkEnableOption "Firefox Configs";

  config = lib.mkIf config.firefox.enable {
    programs.firefox = {
      enable = true;
      profiles.default = {
        extensions = with pkgs.nur.repos.rycee.firefox-addons; [
          ublock-origin
          bitwarden
          tokyo-night-v2
          dearrow
          sponsorblock
        ];
        search = {
          default = "Startpage";
          engines."Startpage".urls = [
            {
              template = "https://www.startpage.com/sp/search";
              params = [
                {
                  name = "q";
                  value = "{searchTerms}";
                }
                {
                  name = "prfe";
                  value = "9f7fa4f5f25493d9fa690811a13aba58c0369af1031d9d498aed816d30f0b00808c38fd3c9349b8e18ac8eec97a0729ebda95dbf77da8dbe399ad46ae98144f35f5885140f0f84abd310eb6d";
                }
              ];
            }
          ];
          force = true;
        };
      };
    };
  };
}

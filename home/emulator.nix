{
  config,
  lib,
  pkgs,
  ...
}:
{
  options.emulator.enable = lib.mkEnableOption "Emulator Configs";

  config = lib.mkIf config.emulator.enable { home.packages = with pkgs; [ mgba ]; };
}

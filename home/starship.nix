{ ... }:
{
  programs.starship = {
    enable = true;
    enableFishIntegration = true;
    enableBashIntegration = true;
    enableNushellIntegration = true;
    settings = {
      add_newline = false;
      status = {
        disabled = false;
      };
      directory = {
        truncation_length = 5;
        truncation_symbol = ".../";
        repo_root_style = "bold underline cyan";
      };
      package = {
        display_private = true;
      };
      memory_usage = {
        threshold = 0;
        disabled = false;
      };
      battery = {
        display = [
          {
            threshold = 10;
            style = "bold red";
          }
          {
            threshold = 25;
            style = "bold yellow";
          }
        ];
      };
    };
  };
}

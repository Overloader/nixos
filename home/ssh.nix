{ pkgs, ... }:
{
  home = {
    packages = with pkgs; [ openssh ];

    shellAliases = {
      ssh-studver = "ssh lukasvoreck@207.180.233.226 -p 2222";
      ssh-studver-tunnel = "ssh lukasvoreck@207.180.233.226 -p 2222 -N -L 1389:localhost:6389";
    };
  };
}

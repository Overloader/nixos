{ pkgs, lib }:
rec {
  all_images = pkgs.stdenv.mkDerivation rec {
    pname = "all_images";
    version = "1.0";
    srcs = [
      (pkgs.fetchurl {
        name = "Nix-Logo.png";
        url = "https://cdn.imgchest.com/files/myd5cgwl3r4.png";
        hash = "sha256-2sT6b49/iClTs9QuUvpmZ5gcIeXI9kebs5IqgQN1RL8=";
      })
      (pkgs.fetchurl {
        name = "Shuttle-Pad.jpg";
        url = "https://cdn.imgchest.com/files/j7mmc3b8g37.jpg";
        hash = "sha256-b+RUhxwUqHR3/gpEldNFvkbpUQXxwzrMCnIdKfWzVxQ=";
      })
      (pkgs.fetchurl {
        name = "SaturnV.jpg";
        url = "https://cdn.imgchest.com/files/k739cgqm5j7.jpg";
        hash = "sha256-s8RAYU2J6U6IysqCdv4H3kEViFOMXqswPIW+j+VzLUM=";
      })
      (pkgs.fetchurl {
        name = "PaleHeart.jpg";
        url = "https://cdn.imgchest.com/files/w7w6czkonay.jpg";
        hash = "sha256-e93T242kAZBXIlMijh77bGmgIACw/l6PeY4/1Yq3XEE=";
      })
      (pkgs.fetchurl {
        name = "Booster-catch.png";
        url = "https://cdn.imgchest.com/files/pyvdcqwke9y.png";
        hash = "sha256-5YwLpYELbXYVKkoPvg0Bx/WV+EvC40Fwpu5SLpgGIXE=";
      })
    ];
    unpackPhase = "true";
    buildPhase = ''
      mkdir -p $out/images
      cd $out/images
      for image in ${lib.concatStringsSep " " (map (s: "\"${s}\"") srcs)}; do
        ln -s $image ./
      done
    '';
    installPhase = "true";
  };
  random_image = pkgs.writeShellScriptBin "random_image" ''
    images=${all_images}/images
    img=$(ls -1 $images | shuf -n 1)
    echo "''${images}/''${img}"
  '';
}

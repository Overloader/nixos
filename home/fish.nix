{ pkgs, ... }:
{
  programs.fish = {
    enable = true;
    plugins = [
      {
        name = "bass";
        src = pkgs.fishPlugins.bass.src;
      }
    ];
    interactiveShellInit = ''
      set fish_greeting
      set -x GPG_TTY (tty)
      set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
      gpgconf --launch gpg-agent

      set_color blue
      echo -n "          ▗▄▄▄       "
      set_color cyan
      echo "▗▄▄▄▄    ▄▄▄▖"

      set_color blue
      echo -n "          ▜███▙       "
      set_color cyan
      echo "▜███▙  ▟███▛"

      set_color blue
      echo -n "           ▜███▙       "
      set_color cyan
      echo "▜███▙▟███▛"

      set_color blue
      echo -n "            ▜███▙       "
      set_color cyan
      echo "▜██████▛"

      set_color blue
      echo -n "     ▟█████████████████▙ "
      set_color cyan
      echo -n "▜████▛     "
      set_color blue
      echo "▟▙"

      set_color blue
      echo -n "    ▟███████████████████▙ "
      set_color cyan
      echo -n "▜███▙    "
      set_color blue
      echo "▟██▙"

      set_color cyan
      echo -n "           ▄▄▄▄▖           ▜███▙  "
      set_color blue
      echo "▟███▛"

      set_color cyan
      echo -n "          ▟███▛             ▜██▛ "
      set_color blue
      echo "▟███▛"

      set_color cyan
      echo -n "         ▟███▛               ▜▛ "
      set_color blue
      echo "▟███▛"

      set_color cyan
      echo -n "▟███████████▛                  "
      set_color blue
      echo "▟██████████▙"

      set_color cyan
      echo -n "▜██████████▛                  "
      set_color blue
      echo "▟███████████▛"

      set_color cyan
      echo -n "      ▟███▛ "
      set_color blue
      echo "▟▙               ▟███▛"

      set_color cyan
      echo -n "     ▟███▛ "
      set_color blue
      echo "▟██▙             ▟███▛"

      set_color cyan
      echo -n "    ▟███▛  "
      set_color blue
      echo "▜███▙           ▝▀▀▀▀"

      set_color cyan
      echo -n "    ▜██▛    "
      set_color blue
      echo -n "▜███▙ "
      set_color cyan
      echo "▜██████████████████▛"

      set_color cyan
      echo -n "     ▜▛     "
      set_color blue
      echo -n "▟████▙ "
      set_color cyan
      echo "▜████████████████▛"

      set_color blue
      echo -n "           ▟██████▙       "
      set_color cyan
      echo "▜███▙"

      set_color blue
      echo -n "          ▟███▛▜███▙       "
      set_color cyan
      echo "▜███▙"

      set_color blue
      echo -n "         ▟███▛  ▜███▙       "
      set_color cyan
      echo "▜███▙"

      set_color blue
      echo -n "         ▝▀▀▀    ▀▀▀▀▘       "
      set_color cyan
      echo "▀▀▀▘"

      set_color normal

      function mark_prompt_start --on-event fish_prompt
          echo -en "\e]133;A\e\\"
      end

      function foot_cmd_start --on-event fish_preexec
        echo -en "\e]133;C\e\\"
      end

      function foot_cmd_end --on-event fish_postexec
        echo -en "\e]133;D\e\\"
      end
    '';
  };
  home = {
    shellAliases = {
      nix-fish = "nix-shell --command fish";
    };
  };
}

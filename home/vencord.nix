{
  config,
  lib,
  pkgs,
  ...
}:
{
  options.vencord.enable = lib.mkEnableOption "vencord Specific Configs";

  config = lib.mkIf config.vencord.enable {
    home = {
      packages = with pkgs; [ vesktop ];
      # Symlink files like this so the ui can edit them
      activation = {
        vesktop_settings_json = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
          mkdir -p ~/.config/vesktop
          run ln -fs $VERBOSE_ARG \
              ~/.config/nixos/home/vesktop/settings.json ~/.config/vesktop/settings.json
        '';
        vesktop_settings_settings_json = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
          mkdir -p ~/.config/vesktop/settings
          run ln -fs $VERBOSE_ARG \
              ~/.config/nixos/home/vesktop/settings/settings.json ~/.config/vesktop/settings/settings.json
        '';
      };
    };
    wayland.windowManager.hyprland.settings = {
      bind = [ "$mainMod, C, exec, vesktop" ];
      windowrule = "workspace special:1,^(vesktop)$";
    };
  };
}

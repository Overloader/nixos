{
  config,
  pkgs,
  lib,
  ...
}:
{
  options.device.lnix.enable = lib.mkEnableOption "Lnix Specific Configs";

  config = lib.mkIf config.device.lnix.enable {
    home.packages = with pkgs; [
      xournalpp
    ];

    wayland.windowManager.hyprland = {
      settings = {
        monitor = "eDP-1,1920x1080,0x0,1";

        input = {
          kb_layout = "de";
        };

        # auto suspend
        bindl = ", switch:Lid Switch, exec, random_lock";
      };
    };

    device.desktop.enable = true;
  };
}

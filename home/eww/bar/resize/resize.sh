#!/usr/bin/env nix-shell
#! nix-shell -i bash 
#! nix-shell -p socat

socat -u UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock - | while read -r event; do
if [[ $event == submap* ]] ; then
    res=${event#"submap>>"}
    [[ $res != resize ]]
    echo $?
fi
done

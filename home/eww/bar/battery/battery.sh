#!/usr/bin/env bash

upower -i $(upower -e | grep 'BAT') | awk '/percentage/ {p=$2} /state/ {s=$2} /time to (empty|full)/ {t=$4" "$5" "$6} END {gsub(/%/,"",p); gsub(/minutes/,"min",t); gsub(/hours/,"h",t); gsub(/^[ \t]+|[ \t]+$/, "", t); printf "{\"percentage\":%.0f,\"state\":\"%s\",\"time\":\"%s\"}",p,s,t}'

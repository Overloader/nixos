#!/usr/bin/env nix-shell
#! nix-shell -i bash
#! nix-shell -p socat jq

hyprctl workspaces -j | jq -c '[.[] | select(.id >= 0) | .id ] | sort'
socat -u UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock - | while read -r line; do
    hyprctl workspaces -j | jq -c '[.[] | select(.id >= 0) | .id ] | sort'
done

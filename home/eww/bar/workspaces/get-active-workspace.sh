#! /usr/bin/env nix-shell
#! nix-shell -i bash
#! nix-shell -p socat jq

hyprctl monitors -j | jq --raw-output .[0].activeWorkspace.id
workspaces() {
    if [[ ${1:0:11} == "workspace>>" ]]; then
        echo "${1:11}"
    elif [[ ${1:0:10} == "focusedmon" ]]; then
        string=${1:11}
        echo ${string##*,}
    fi
}

socat -u UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock - | while read -r event; do
    workspaces "$event"
done

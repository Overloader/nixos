#!/usr/bin/env nix-shell
#! nix-shell -i bash
#! nix-shell -p socat jq

hyprctl workspaces -j | jq -c '[.[] | select(.name | startswith("special:")) | {id: (.name | split(":")[1] | tonumber), lastwindowtitle: .lastwindowtitle}] | sort_by(.id)'
socat -u UNIX-CONNECT:$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock - | while read -r line; do
    hyprctl workspaces -j | jq -c '[.[] | select(.name | startswith("special:")) | {id: (.name | split(":")[1] | tonumber), lastwindowtitle: .lastwindowtitle}] | sort_by(.id)'
done

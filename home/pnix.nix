{
  config,
  pkgs,
  lib,
  ...
}:
{
  options.device.pnix.enable = lib.mkEnableOption "Pnix Specific Configs";

  config = lib.mkIf config.device.pnix.enable {
    home.packages = with pkgs; [ heroic ];
    wayland.windowManager.hyprland.settings = {
      monitor = [
        "HDMI-A-3,1920x1080,0x360,1"
        "DP-3,2560x1440,1920x0,1"
        "Unknown-1,disabled"
      ];
      env = [
        "LIBVA_DRIVER_NAME,nvidia"
        "XDG_SESSION_TYPE,wayland"
        "GBM_BACKEND,nvidia-drm"
        "WLR_NO_HARDWARE_CURSORS,1"
        "__GLX_VENDOR_LIBRARY_NAME,nvidia"
      ];
    };
    xsession.windowManager.i3.config.startup = [
      {
        command = "xrandr --output HDMI-0 --left-of DP-4";
        notification = false;
      }
    ];

    device.desktop.enable = true;
  };
}

{
  config,
  lib,
  pkgs,
  ...
}:
{
  options.device.desktop.enable = lib.mkEnableOption "Desktop Specific Configs";

  config = lib.mkIf config.device.desktop.enable {
    hyprland.enable = true;
    foot.enable = false;
    kitty.enable = true;
    firefox.enable = true;
    chrome.enable = true;
    i3.enable = false;
    emulator.enable = true;
    vencord.enable = true;

    gtk = {
      enable = true;
      theme = {
        name = "Dark";
        package = pkgs.tokyonight-gtk-theme;
      };
    };

    home.packages = with pkgs; [
      maliit-keyboard
      anki
      bitwarden
      blueman
      prismlauncher
      obs-studio
      libreoffice
      tor-browser-bundle-bin
      element-desktop
      gimp
      helvum
      noisetorch

      lutris
      wine
    ];
  };
}

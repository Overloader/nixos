{
  pkgs,
  lib,
  config,
  ...
}:
{
  options.rofi.enable = lib.mkEnableOption "Rofi Configs";

  config = lib.mkIf config.rofi.enable {
    programs.rofi = {
      enable = true;
      location = "top";
      theme = "Arc-Dark";
      yoffset = 30;
    };
    home.packages = with pkgs; [ rofimoji ];
  };
}

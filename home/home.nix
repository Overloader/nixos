{ pkgs, nixosConfig, ... }:
{
  imports = [
    ../vim/vim.nix
    ./hyprland/imports.nix
    ./fish.nix
    ./foot.nix
    ./alacritty.nix
    ./kitty.nix
    ./git.nix
    ./starship.nix
    ./emulator.nix
    ./lsd.nix
    ./firefox.nix
    ./chrome.nix
    ./zellij.nix
    ./lnix.nix
    ./pnix.nix
    ./ssh.nix
    ./desktop.nix
    ./i3.nix
    ./direnv.nix
    ./vencord.nix
  ];

  home = {
    # Home Manager needs a bit of information about you and the paths it should
    # manage.
    username = "lukas";
    homeDirectory = "/home/lukas";

    packages = with pkgs; [
      bat
      openssh
      lazygit
      btop
      pulsemixer
      pulseaudio
      unzip
      zip
      pinentry-qt
    ];

    sessionVariables = rec {
      # Declutter Home
      HOME = "/home/lukas";
      XDG_DATA_HOME = "${HOME}/.local/share";
      XDG_CONFIG_HOME = "${HOME}/.config";
      XDG_STATE_HOME = "${HOME}/.local/state";
      XDG_CACHE_HOME = "${HOME}/.cache";
      HISTFILE = "${XDG_STATE_HOME}/bash/history";
      XCOMPOSECACHE = "${XDG_CACHE_HOME}/X11/xcompose";
      ERRFILE = "${XDG_CACHE_HOME}/x11/xsession-errors";
      GNUPGHOME = "${XDG_DATA_HOME}/gnupg";
      PYTHONSTARTUP = "${XDG_CONFIG_HOME}/python/pythonrc";
    };

    file.".config/python/pythonrc".text = ''
      import osj
      import atexit
      import readline
      from pathlib import Path

      if readline.get_current_history_length() == 0:

          state_home = os.environ.get("XDG_STATE_HOME")
          if state_home is None:
              state_home = Path.home() / ".local" / "state"
          else:
              state_home = Path(state_home)

          history_path = state_home / "python_history"
          if history_path.is_dir():
              raise OSError(f"'{history_path}' cannot be a directory")

          history = str(history_path)

          try:
              readline.read_history_file(history)
          except OSError: # Non existent
              pass

          def write_history():
              try:
                  readline.write_history_file(history)
              except OSError:
                  pass

          atexit.register(write_history)
    '';

    # This value determines the Home Manager release that your configuration is
    # compatible with. This helps avoid breakage when a new Home Manager release
    # introduces backwards incompatible changes.
    #
    # You should not change this value, even if you update Home Manager. If you do
    # want to update the value, then make sure to first check the Home Manager
    # release notes.
    stateVersion = "23.05"; # Please read the comment before changing.
  };

  device = nixosConfig.device;

  programs = {
    # Let Home Manager install and manage itself.
    home-manager.enable = true;

    atuin.enable = true;
    command-not-found.enable = false;
  };

  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = [ "qemu:///system" ];
      uris = [ "qemu:///system" ];
    };
  };
}

{
  config,
  lib,
  pkgs,
  ...
}:
{

  options.i3.enable = lib.mkEnableOption "i3 Configs";

  config = lib.mkIf config.i3.enable {
    home.packages = with pkgs; [
      rofi-power-menu
      haskellPackages.greenclip
      flameshot
      feh
    ];
    xsession.windowManager.i3 = {
      enable = true;
      config = {
        keybindings =
          let
            modifier = "Mod4";
          in
          {
            "${modifier}+Return" = "exec i3-sensible-terminal";
            "${modifier}+Shift+q" = "kill";
            "${modifier}+d" = "exec rofi -show drun -show-icons";

            "${modifier}+h" = "focus left";
            "${modifier}+j" = "focus down";
            "${modifier}+k" = "focus up";
            "${modifier}+l" = "focus right";

            "${modifier}+Shift+h" = "move left";
            "${modifier}+Shift+j" = "move down";
            "${modifier}+Shift+k" = "move up";
            "${modifier}+Shift+l" = "move right";

            "${modifier}+f" = "fullscreen toggle";

            "${modifier}+s" = "layout stacking";
            "${modifier}+w" = "layout tabbed";
            "${modifier}+e" = "layout toggle split";

            "${modifier}+Shift+space" = "floating toggle";

            "${modifier}+1" = "workspace number 1";
            "${modifier}+2" = "workspace number 2";
            "${modifier}+3" = "workspace number 3";
            "${modifier}+4" = "workspace number 4";
            "${modifier}+5" = "workspace number 5";
            "${modifier}+6" = "workspace number 6";
            "${modifier}+7" = "workspace number 7";
            "${modifier}+8" = "workspace number 8";
            "${modifier}+9" = "workspace number 9";
            "${modifier}+0" = "workspace number 10";

            "${modifier}+Shift+1" = "move container to workspace number 1";
            "${modifier}+Shift+2" = "move container to workspace number 2";
            "${modifier}+Shift+3" = "move container to workspace number 3";
            "${modifier}+Shift+4" = "move container to workspace number 4";
            "${modifier}+Shift+5" = "move container to workspace number 5";
            "${modifier}+Shift+6" = "move container to workspace number 6";
            "${modifier}+Shift+7" = "move container to workspace number 7";
            "${modifier}+Shift+8" = "move container to workspace number 8";
            "${modifier}+Shift+9" = "move container to workspace number 9";
            "${modifier}+Shift+0" = "move container to workspace number 10";

            "${modifier}+End" = "exec rofi -show power-menu -modi power-menu:rofi-power-menu";

            "${modifier}+r" = "mode \"resize\"";

            "${modifier}+v" = "exec --no-startup-id rofi -modi \"clipboard:greenclip pring\" -show clipboard";

            "${modifier}+i" = "exec firefox";
            "${modifier}+c" = "exec discord";

            "Print" = "exec flameshot gui";

            "${modifier}+period" = "exec rofimoji > /dev/null";
          };
        bars = [
          {
            colors = {
              background = "#282A36";
              statusline = "#F8F8F2";
              separator = "#44475A";

              focusedWorkspace = {
                border = "#44475A";
                background = "#44475A";
                text = "#F8F8F2";
              };
              activeWorkspace = {
                border = "#282A36";
                background = "#44475A";
                text = "#F8F8F2";
              };
              inactiveWorkspace = {
                border = "#282A36";
                background = "#282A36";
                text = "#BFBFBF";
              };
              urgentWorkspace = {
                border = "#FF5555";
                background = "#FF5555";
                text = "#F8F8F2";
              };
              bindingMode = {
                border = "#FF5555";
                background = "#FF5555";
                text = "#F8F8F2";
              };
            };
            extraConfig = "status_command i3status-rs ~/.config/i3status-rust/config-default.toml";
            fonts = {
              names = [ "Fira Code" ];
              size = 10.0;
            };
            position = "top";
            trayOutput = "primary";
          }
        ];
        colors = {
          focused = {
            border = "#9aa5ce";
            background = "#364A82";
            text = "#C0CAF5";
            indicator = "#9AA5CE";
            childBorder = "#9AA5CE";
          };
          focusedInactive = {
            border = "#16161D";
            background = "#16161D";
            text = "#C0CAF5";
            indicator = "#16161D";
            childBorder = "#16161D";
          };
          unfocused = {
            border = "#16161D";
            background = "#16161D";
            text = "#C0CAF5";
            indicator = "#16161D";
            childBorder = "#16161D";
          };
        };
        floating.modifier = config.xsession.windowManager.i3.config.modifier;
        focus = {
          followMouse = true;
          mouseWarping = true;
        };
        fonts = {
          names = [ "Fira Code" ];
          size = 8.0;
        };
        menu = "rofi -show drun -show-icons";
        modes = {
          resize =
            let
              modifier = "Mod4";
            in
            {
              "h" = "resize shrink width 10 px or 10 ppt";
              "j" = "resize grow height 10 px or 10 ppt";
              "k" = "resize shrink height 10 px or 10 ppt";
              "l" = "resize grow width 10 px or 10 ppt";

              "${modifier}+Escape" = "mode \"default\"";
              "${modifier}+r" = "mode \"default\"";
            };
        };
        startup = [
          {
            command = "dunst";
            notification = false;
          }
          {
            command = "feh --bg-fill ~/Pictures/wallpaper.jpg --no-fehbg";
            notification = false;
          }
        ];
        window.titlebar = false;
      };
    };
    programs.i3status-rust = {
      enable = true;
      bars.default = {
        icons = "awesome6";
        theme = "dracula";
        blocks = [
          { block = "cpu"; }
          {
            block = "memory";
            format = "$icon $mem_used.eng(prefix:M)/$mem_total.eng(prefix:M) ($mem_total_used_percents.eng(w:2))";
            format_alt = "$icon_swap $swap_used.eng(prefix:M)/$swap_total.eng(prefix:M) ($swap_used_percents.eng(w:2))";
            interval = 1;
          }
          {
            block = "sound";
            driver = "pulseaudio";
          }
          {
            block = "time";
            format = " $icon $timestamp.datetime(f:'%Y-%m-%d %R', l:fr_BE) ";
            interval = 1;
          }
        ];
      };
    };
  };
}

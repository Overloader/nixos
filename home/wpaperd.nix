{
  config,
  lib,
  pkgs,
  ...
}:
let
  pictures = import ./pictures.nix { inherit pkgs lib; };
in
{

  options.wpaperd.enable = lib.mkEnableOption "wpaperd Configs";

  config = lib.mkIf config.wpaperd.enable {
    home.packages = with pkgs; [ wpaperd ];

    home.file.".config/wpaperd/config.toml".text = ''
      [default]
      duration = "30m"
      mode = "center"
      sorting = "random"

      [any]
      path = "${pictures.all_images}/images"
    '';
  };
}

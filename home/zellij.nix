{ ... }:
{
  # TODO: Propper configuration
  programs = {
    zellij = {
      enable = true;
      settings = {
        themes = {
          default = {
            fg = "#C0CAF5";
            bg = "#24283B";
            black = "#414868";
            red = "#F7765E";
            green = "#9ECE6A";
            yellow = "#E0AF68";
            blue = "#2AC3DE";
            magenta = "#BB9AF7";
            cyan = "#7DCFFF";
            white = "#FFFFFF";
            orange = "#FF9E64";
          };
        };
        on_force_close = "quit";
        copy_on_select = false;
        ui = {
          pane_frames = {
            rounded_corners = true;
          };
        };
      };
    };
  };
}

{
  pkgs,
  lib,
  config,
  ...
}:
{
  options.fuzzel.enable = lib.mkEnableOption "Fuzzel Configs";

  config = lib.mkIf config.fuzzel.enable {
    programs.fuzzel = {
      enable = true;
      settings = {
        colors = {
          background = "1a1b26ff";
          text = "c0caf5ff";
          match = "73dacaff";

          selection = "283457ff";
          selection-match = "73dacaff";
          selection-text = "c0caf5ff";

          border = "db4b4bff";
        };
        main = {
          anchor = "top";
          lines = 20;
          width = 50;
        };
      };
    };
    home = {
      packages = with pkgs; [ bemoji ];
      sessionVariables = {
        BEMOJI_PICKER_CMD = "fuzzel -d";
      };
    };
  };
}

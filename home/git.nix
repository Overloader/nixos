{ ... }:
{
  programs.git = {
    enable = true;
    lfs.enable = true;
    userEmail = "overloader@tutanota.com";
    userName = "Overloader";
    signing = {
      key = "9577680CA408B184";
      signByDefault = true;
    };
    extraConfig = {
      merge.tool = "nvim";
      mergetool."nvim".cmd = ''nvim -d -c "wincmd l" -c "norm ]c" "$LOCAL" "$MERGED" "$REMOTE"'';
    };
  };
}

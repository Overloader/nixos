{
  pkgs,
  lib,
  config,
  ...
}:
{

  options.eww.enable = lib.mkEnableOption "Eww Configs";

  config = lib.mkIf config.eww.enable {
    programs.eww = {
      enable = true;
      configDir = ./eww;
    };
    home.packages = with pkgs; [
      sass
      socat
      jq
    ];
  };
}

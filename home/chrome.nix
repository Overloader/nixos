{
  config,
  lib,
  pkgs,
  ...
}:
{
  options.chrome.enable = lib.mkEnableOption "Chrome Configs";

  config = lib.mkIf config.chrome.enable {
    programs.chromium = rec {
      enable = true;
      package = pkgs.ungoogled-chromium;
      extensions =
        let
          createChromiumExtensionFor =
            browserVersion:
            {
              id,
              sha256,
              version,
            }:
            {
              inherit id;
              crxPath = builtins.fetchurl {
                url = "https://clients2.google.com/service/update2/crx?response=redirect&acceptformat=crx2,crx3&prodversion=${browserVersion}&x=id%3D${id}%26installsource%3Dondemand%26uc";
                name = "${id}.crx";
                inherit sha256;
              };
              inherit version;
            };
          createChromiumExtension = createChromiumExtensionFor (lib.versions.major package.version);
        in
        [
          (createChromiumExtension {
            # ublock
            id = "cjpalhdlnbpafiamejdnhcphjbkeiagm";
            sha256 = "0ycnkna72n969crgxfy2lc1qbndjqrj46b9gr5l9b7pgfxi5q0ll";
            version = "1.62.0";
          })
          (createChromiumExtension {
            # Bitwarden
            id = "nngceckbapebfimnlniiiahkandclblb";
            sha256 = "1vsvswam4bz0j1sc7ig0xnysshjwj4x7nnzlic711jasf5c3sg3p";
            version = "2025.2.1";
          })
          (createChromiumExtension {
            # Sponsorblock
            id = "mnjggcdmjocbbbhaepdhchncahnbgone";
            sha256 = "07g8278s55wzzjb7im3lirmq7aq0pb3zfgw8xly0wczwjz3mhzb6";
            version = "5.11.5";
          })
        ];
      # defaultSearchProviderSuggestURL = "https://www.startpage.com/sp/search?q={searchTerms}&prfe=9f7fa4f5f25493d9fa690811a13aba58c0369af1031d9d498aed816d30f0b00808c38fd3c9349b8e18ac8eec97a0729ebda95dbf77da8dbe399ad46ae98144f35f5885140f0f84abd310eb6d";
      # defaultSearchProviderSearchURL = defaultSearchProviderSuggestURL;
    };
  };
}

{
  config,
  lib,
  pkgs,
  ...
}:
let
  pictures = import ./pictures.nix { inherit pkgs lib; };
  random_wallpaper = (
    pkgs.writeShellScriptBin "random_background" ''
      image=$(random_image)
      monitor=(`hyprctl monitors | grep Monitor | awk '{print $2}'`)
      hyprctl hyprpaper unload all
      hyprctl hyprpaper preload $image
      for m in ''${monitor[@]}; do
        hyprctl hyprpaper wallpaper "$m,$image"
      done
    ''
  );
in
{

  options.hyprpaper.enable = lib.mkEnableOption "Hyprpaper Configs";

  config = lib.mkIf config.hyprpaper.enable {
    home.packages = with pkgs; [
      pictures.random_image
      random_wallpaper
      hyprpaper
    ];

    systemd.user = {
      services.wallpaperRandomizer = {
        Install = {
          WantedBy = [ "graphical-session.target" ];
        };

        Unit = {
          Description = "Set random desktop background using hyprpaper";
          After = [ "graphical-session-pre.target" ];
          PartOf = [ "graphical-session.target" ];
        };

        Service = {
          Type = "oneshot";
          ExecStart = "${random_wallpaper}/bin/wallpaperRandomizer";
          IOSchedulingClass = "idle";
        };
      };

      timers.wallpaperRandomizer = {
        Unit = {
          Description = "Set random desktop background using hyprpaper on an interval";
        };

        Timer = {
          OnUnitActiveSec = "1h";
        };

        Install = {
          WantedBy = [ "timers.target" ];
        };
      };
    };
  };
}

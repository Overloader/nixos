{ ... }:
{
  imports = [
    ../eww.nix
    ../hyprpaper.nix
    ../wpaperd.nix
    ../swaylock.nix
    ../fuzzel.nix

    ./module.nix
  ];
}

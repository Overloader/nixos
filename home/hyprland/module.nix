{
  pkgs,
  lib,
  config,
  ...
}:
let
  pictures = import ../pictures.nix { inherit pkgs lib; };
in
{
  options.hyprland.enable = lib.mkEnableOption "Hyprland Configs";

  config = lib.mkIf config.hyprland.enable {
    eww.enable = true;
    hyprpaper.enable = false;
    wpaperd.enable = true;
    swaylock.enable = true;
    fuzzel.enable = true;
    home.packages = with pkgs; [
      cliphist
      wl-clipboard
      hyprpolkitagent
      sway-contrib.grimshot
      xdg-desktop-portal-hyprland
      swaynotificationcenter
      pictures.random_image
    ];
    wayland.windowManager.hyprland = {
      enable = true;
      settings =
        let
          colors = {
            cyan = "rgb(7dcfff)";
            magenta = "rgb(bb9af7)";
            purple = "rgb(9d7cd8)";
            orange = "rgb(ff9e64)";
            red = "rgb(f7768e)";
          };
        in
        {
          exec-once = [
            "wpaperd -d"
            "exec swaync"
            "eww daemon"
            "eww open bar0"
            "eww open bar1"
            "wl-paste --type text --watch cliphist store"
            "wl-paste --type image --watch cliphist store"
            "systemctl --user start hyprpolkitagent"
          ];

          general = {
            border_size = 2;
            gaps_in = 3;
            gaps_out = 10;
            "col.inactive_border" = "${colors.cyan} ${colors.purple}";
            "col.active_border" = "${colors.red} ${colors.orange}";
            layout = "master";
          };

          group = {
            "col.border_inactive" = "${colors.purple} ${colors.magenta}";
            "col.border_active" = "${colors.red} ${colors.magenta}";
            groupbar = {
              "col.inactive" = "${colors.purple} ${colors.magenta}";
              "col.active" = "${colors.red} ${colors.magenta}";
            };
          };

          decoration = {
            rounding = 9;
            dim_inactive = true;
            dim_strength = 0.2;
          };

          animations = {
            enabled = true;

            bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";

            animation = [
              "windows, 1, 7, myBezier"
              "windowsOut, 1, 7, default, popin 80%"
              "border, 1, 1, default"
              "borderangle, 1, 10, default"
              "fade, 1, 7, default"
              "workspaces, 1, 6, default"
            ];
          };

          "$mainMod" = "SUPER";

          bind =
            [
              "$mainMod, Return, exec, kitty"
              "$mainMod_SHIFT, Q, killactive,"
              "$mainMod_SHIFT, Space, togglefloating,"
              "$mainMod, D, exec, fuzzel"
              "$mainMod, D, movecursortocorner, 3"
              "$mainMod, I, exec, chromium"
              "$mainMod, V, exec, cliphist list | fuzzel --dmenu | cliphist decode | wl-copy"
              "$mainMod, V, movecursortocorner, 3"
              ", Print, exec, grimshot copy area"
              ", XF86MonBrightnessDown, exec, light -U 5"
              ", XF86MonBrightnessUp, exec, light -A 5"
              ", XF86AudioRaiseVolume, exec, pactl set-sink-volume 0 +1%"
              ", XF86AudioLowerVolume, exec, pactl set-sink-volume 0 -1%"
              ", XF86AudioMute, exec, pactl set-sink-mute 0 toggle"
              "$mainMod, period, exec, bemoji"
              "$mainMod, period, movecursortocorner, 3"

              # Move focus
              "$mainMod, H, movefocus, l"
              "$mainMod, L, movefocus, r"
              "$mainMod, J, movefocus, u"
              "$mainMod, K, movefocus, d"

              # Move window
              "$mainMod SHIFT, H, movewindow, l"
              "$mainMod SHIFT, L, movewindow, r"
              "$mainMod SHIFT, J, movewindow, u"
              "$mainMod SHIFT, K, movewindow, d"

              # Fullscreen and maximize
              "$mainMod SHIFT, f, fullscreen, 0"
              "$mainMod, f, fullscreen, 1"
              "$mainMod ALT, f, fullscreenstate, -1 2"

              # Groups
              "$mainMod, g, togglegroup"
              "$mainMod SHIFT, g, moveoutofgroup"
              "$mainMod ALT, h, moveintogroup, l"
              "$mainMod ALT, j, moveintogroup, d"
              "$mainMod ALT, k, moveintogroup, u"
              "$mainMod ALT, l, moveintogroup, r"
              "$mainMod, a, changegroupactive, b"
              "$mainMod, s, changegroupactive, f"

              # Resizing
              "$mainMod, R, submap, resize"

              # shutdown
              "$mainMod, end, exec, eww open shutdown-menu"
              "$mainMod, end, submap, shutdown"

              # Notifications
              "$mainMod SHIFT, n, exec, swaync-client -t -sw"

              # Editing
              "$mainMod, e, exec, neovide"
            ]
            ++ (
              # Workspaces
              builtins.concatLists (
                builtins.genList (
                  x:
                  let
                    ws =
                      let
                        c = (x + 1) / 10;
                      in
                      builtins.toString (x + 1 - (c * 10));
                  in
                  [
                    # Switch to Workspace
                    "$mainMod, ${ws}, workspace, ${toString (x + 1)}"

                    # Move active window to workspace
                    "$mainMod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"

                    # Move silently
                    "$mainMod SHIFT ALT, ${ws}, movetoworkspacesilent, ${toString (x + 1)}"
                  ]
                ) 10
              )
            )
            ++ (
              # special Workspaces
              builtins.concatLists (
                builtins.genList (x: [
                  # toggle Workspace
                  "$mainMod, f${toString (x + 1)}, togglespecialworkspace, ${toString (x + 1)}"

                  # Move active window to special workspace
                  "$mainMod SHIFT, f${toString (x + 1)}, movetoworkspace, special:${toString (x + 1)}"
                ]) 10
              )
            );

          binde = [
            # move floating windows
            "$mainMod ALT, l, moveactive, 10 0"
            "$mainMod ALT, h, moveactive, -10 0"
            "$mainMod ALT, j, moveactive, 0 -10"
            "$mainMod ALT, k, moveactive, 0 10"
          ];
        };

      extraConfig = ''
        submap = resize
        binde = , l, resizeactive, 10 0
        binde = , h, resizeactive, -10 0
        binde = , k, resizeactive, 0 -10
        binde = , j, resizeactive, 0 10
        bind = , escape, submap, reset
        bind = $mainMod, R, submap, reset
        submap = reset

        submap = shutdown
        bind = , s, exec, shutdown now
        bind = , r, exec, reboot
        bind = SHIFT, l, exec, loginctl sesssion-status | head -n 1 | awk '{print $1}' | xargs loginctl terminate-session
        bind = , l, exec, eww close shutdown-menu && hyprctl dispatch submap reset && random_lock 
        bind = , escape, exec, eww close shutdown-menu
        bind = , escape, submap, reset
        submap = reset
      '';
    };
  };
}

{ lib, config, ... }:
{
  options.kitty.enable = lib.mkEnableOption "Kitty Configs";

  config = lib.mkIf config.kitty.enable {
    programs.kitty = {
      enable = true;
      font.name = "FiraCode Nerd Font";
      themeFile = "tokyo_night_night";
    };
  };
}

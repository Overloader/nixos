{ config, pkgs, ... }:

{
  imports = [
    ./lnix.nix
    ./pnix.nix
  ];

  nix = {
    settings.experimental-features = [
      "nix-command"
      "flakes"
    ];
    optimise.automatic = true;
    gc.automatic = true;
  };

  # Enable networking
  networking.networkmanager.enable = true;
  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";

    extraLocaleSettings = {
      LC_ADDRESS = "de_DE.UTF-8";
      LC_IDENTIFICATION = "de_DE.UTF-8";
      LC_MEASUREMENT = "de_DE.UTF-8";
      LC_MONETARY = "de_DE.UTF-8";
      LC_NAME = "de_DE.UTF-8";
      LC_NUMERIC = "de_DE.UTF-8";
      LC_PAPER = "de_DE.UTF-8";
      LC_TELEPHONE = "de_DE.UTF-8";
      LC_TIME = "de_DE.UTF-8";
    };
  };

  services = {
    flatpak.enable = true;
    # Enable the X11 windowing system.
    xserver = {
      enable = true;
      excludePackages = [ pkgs.xterm ];
      displayManager = {
        lightdm = {
          enable = true;
          extraConfig = "user-authority-in-system-dir=true";
        };
      };
    };
    desktopManager.plasma6.enable = true;
    displayManager = {
      defaultSession = "hyprland";
      # Enable automatic login for the user.
      autoLogin = {
        enable = true;
        user = "lukas";
      };
    };

    # Enable CUPS to print documents.
    printing.enable = true;

    upower.enable = true;
    pcscd.enable = true;

    # Enable sound with pipewire.
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    blueman.enable = true;
  };
  hardware = {
    pulseaudio.enable = false;
    bluetooth.enable = true;
  };
  security = {
    rtkit.enable = true;
    pam.services.swaylock = { };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.lukas = {
    isNormalUser = true;
    shell = pkgs.fish;
    description = "lukas";
    extraGroups = [
      "networkmanager"
      "wheel"
      "video"
      "adbusers"
      "libvirtd"
    ];
  };

  environment = {
    systemPackages = with pkgs; [
      yubioath-flutter
      yubikey-manager-qt
      virt-manager
      nix-output-monitor
    ];
    plasma6.excludePackages = with pkgs.kdePackages; [ kate ];
    defaultPackages = [ ];
    variables = {
      FLAKE = "/home/lukas/.config/nixos";
    };
  };

  documentation.nixos.enable = false;

  virtualisation = {
    docker = {
      enable = false;
      storageDriver = "btrfs";
    };
    libvirtd.enable = true;
  };

  programs = {
    command-not-found.enable = false;
    dconf.enable = true;
    fish.enable = true;
    hyprland.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
    steam.enable = true;
    gamemode.enable = true;
    adb.enable = true;
    nh = {
      enable = true;
      flake = "/home/lukas/.config/nixos";
    };
  };

  xdg.portal = {
    enable = true;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  fonts.packages = with pkgs; [ (nerdfonts.override { fonts = [ "FiraCode" ]; }) ];

  boot = {
    extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback.out ];
    kernelModules = [ "v4l2loopback" ];
    extraModprobeConfig = ''
      options v4l2loopback exclusive_caps=1 card_label="Virtual Camera"
    '';
    loader = {
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot";
      };
      grub = {
        devices = [ "nodev" ];
        efiSupport = true;
        enable = true;
      };
      grub2-theme = {
        enable = true;
        theme = "stylish";
        footer = true;
      };
    };
    initrd.systemd.enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}

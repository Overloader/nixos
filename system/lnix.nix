{
  config,
  pkgs,
  lib,
  ...
}:
{
  imports = [
    # Include the results of the hardware scan.
    ./lnix-hardware-configuration.nix
  ];

  options.device.lnix.enable = lib.mkEnableOption "Lnix Specific Configs";

  config = lib.mkIf config.device.lnix.enable {
    # GENERATED

    boot.initrd.luks.devices."luks-a4f1992d-383b-469d-8d39-1adbef81b168".device = "/dev/disk/by-uuid/a4f1992d-383b-469d-8d39-1adbef81b168";

    networking.hostName = "lnix"; # Define your hostname.

    # OWN

    services = {
      # Configure keymap in X11
      xserver = {
        xkb = {
          layout = "de";
          variant = "";
        };
      };
      tlp = {
        enable = false;
        settings = {
          CPU_SCALING_GOVERNOR_ON_AC = "performance";
          CPU_SCALING_GOVERNOR_ON_BAT = "powersave";

          CPU_ENERGY_PERF_POLICY_ON_BAT = "power";
          CPU_ENERGY_PERF_POLICY_ON_AC = "performance";

          START_CHARGE_THRESH_BAT0 = 40;
          STOP_CHARGE_THRESH_BAT0 = 90;
        };
      };
    };

    # Configure console keymap
    console.keyMap = "de";

    boot.kernelPackages = pkgs.linuxPackages_latest;

    programs.light.enable = true;
  };
}

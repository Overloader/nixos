{
  pkgs,
  config,
  lib,
  ...
}:
{
  imports = [
    # Include the results of the hardware scan.
    ./pnix-hardware-configuration.nix
  ];

  options.device.pnix.enable = lib.mkEnableOption "Pnix Specific Configs";

  config = lib.mkIf config.device.pnix.enable {
    # GENERATED

    # Setup keyfile
    boot.initrd.secrets = {
      "/crypto_keyfile.bin" = null;
    };

    boot.initrd.luks.devices."luks-f93a7f4f-d52a-4b24-b2dd-9ed552fd84a3".keyFile =
      "/crypto_keyfile.bin";

    networking.hostName = "pnix"; # Define your hostname.

    # OWN

    # Bootloader
    boot.loader.grub.extraEntries = ''
      menuentry "Windows" {
        insmod part_gpt
        insmod fat
        insmod search_fs_uuid
        insmod chain
        search --fs-uuid --set=root $FS_UUID
        chainloader /EFI/Microsoft/Boot/bootmgfw.efi
      }
    '';

    environment.systemPackages = with pkgs; [
      polychromatic
    ];

    console.keyMap = "us";

    services = {
      xserver = {
        # Configure console keymap
        xkb.layout = "us";
        # Nvidia driver
        videoDrivers = [ "nvidia" ];
        xrandrHeads = [
          "HDMI-0"
          {
            output = "DP-4";
            primary = true;
          }
        ];
      };
      hardware.openrgb.enable = true;
    };

    boot.kernelPackages = pkgs.linuxPackages_latest;

    swapDevices = [
      {
        device = "/var/lib/swapfile";
        size = 16 * 1024;
      }
    ];

    hardware = {
      graphics = {
        enable = true;
        enable32Bit = true;
      };

      nvidia = {
        open = false;
        modesetting.enable = true;
        nvidiaSettings = true;
        package = config.boot.kernelPackages.nvidiaPackages.stable;
      };

      openrazer = {
        enable = true;
        users = [ "lukas" ];
      };
    };
  };
}

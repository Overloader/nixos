{ ... }:
{
  programs.nixvim = {
    plugins.oil = {
      enable = true;
      settings.win_options = {
        number = false;
        relativenumber = false;
      };
    };
    keymaps = [
      {
        key = "<leader>fb";
        action.__raw = ''
          function()
            require("oil").open_float(nil, {
              preview = {
                vertical = true
              }
            })
          end
        '';
        options.desc = " File Browser";
      }
    ];
  };
}

{
  pkgs,
  config,
  nixvim,
  ...
}:
{
  imports = [
    nixvim.homeManagerModules.nixvim
    ./ui.nix
    ./coding.nix
    ./filebrowser.nix
    ./telescope.nix
    ./folding.nix
    ./misc.nix
  ];
  programs.nixvim = {
    enable = true;
    defaultEditor = true;
    #nixpkgs.useGlobalPackages = true;
    vimdiffAlias = true;
    vimAlias = true;

    performance.byteCompileLua.enable = true;

    extraPackages = with pkgs; [
      ripgrep
    ];

    enableMan = false;
    diagnostics = {
      severity_sort = true;
      signs.text = {
        "[vim.diagnostic.severity.ERROR]" = " ";
        "[vim.diagnostic.severity.WARN]" = " ";
        "[vim.diagnostic.severity.HINT]" = " ";
        "[vim.diagnostic.severity.INFO]" = " ";
      };
    };

    opts = {
      undofile = true;
      scrolloff = 5;
      signcolumn = "yes:2";
      termguicolors = true;
      cursorline = true;
      conceallevel = 2;
      relativenumber = true;
      number = true;
      splitkeep = "screen";
      splitbelow = true;
      splitright = true;
      ignorecase = true;
      smartcase = true;
      expandtab = true;
      tabstop = 4;
      shiftwidth = 4;
      wrap = true;
      linebreak = true;
      breakindent = true;
      showbreak = " 󱞩";
      completeopt = "menuone,noselect,preview";
      spell = true;
      spelllang = [
        "de"
        "en"
      ];
    };
    globals.mapleader = " ";
    clipboard = {
      register = "unnamedplus";
      providers = {
        wl-copy.enable = true;
        xclip.enable = true;
      };
    };
    keymaps = [
      {
        action.__raw = ''
          function()
            vim.cmd.nohl()
            require("notify").dismiss()
          end
        '';
        key = "<esc>";
        mode = "n";
        options = {
          desc = "Clear search highlight and close notifications";
          silent = true;
        };
      }
      {
        action = "<c-w>l";
        key = "<c-l>";
        options.desc = "Move to right window";
      }
      {
        action = "<c-w>h";
        key = "<c-h>";
        options.desc = "Move to left window";
      }
      {
        action = "<c-w>j";
        key = "<c-j>";
        options.desc = "Move to bottom window";
      }
      {
        action = "<c-w>k";
        key = "<c-k>";
        options.desc = "Move to top window";
      }
      {
        action = "<c-w>k";
        key = "<c-k>";
        options.desc = "Move to top window";
      }
      {
        action = "<cmd>bd<cr>";
        key = "<leader>bd";
        options.desc = " Close Buffer";
      }
      {
        action = ">gv";
        key = ">";
        mode = "v";
      }
      {
        action = "<gv";
        key = "<";
        mode = "v";
      }
      {
        action = "<C-\\><C-n>";
        key = "<C-Esc>";
        mode = "t";
      }
    ];
    autoCmd = [
      {
        callback.__raw = ''
          function()
            vim.highlight.on_yank({ higrou = "IncSearch", timeout = 500 })
          end
        '';
        desc = "Highlights yanked text";
        event = "TextYankPost";
      }
    ];
    colorscheme = "tokyonight";
    colorschemes.tokyonight.enable = true;
    plugins.web-devicons.enable = true;

    extraFiles = {
      "spell/de.utf-8.spl".source = builtins.fetchurl {
        url = "https://ftp.nluug.nl/vim/runtime/spell/de.utf-8.spl";
        sha256 = "1ld3hgv1kpdrl4fjc1wwxgk4v74k8lmbkpi1x7dnr19rldz11ivk";
      };
    };

    extraConfigLua = ''
      vim.env.GNUPGHOME = "${config.home.sessionVariables.GNUPGHOME}";
    '';
  };
  home.packages = [ pkgs.neovide ];
}

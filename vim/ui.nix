{ ... }:
{
  programs.nixvim = {
    plugins = {
      noice = {
        enable = true;
        settings = {
          command_palette = true;
          long_message_to_split = true;
          lsp_doc_border = true;
          lsp.override = {
            "cmp.entry.get_documentation" = false;
            "vim.lsp.util.convert_input_to_markdown_lines" = false;
            "vim.lsp.util.stylize_markdown" = false;
          };
        };
      };
      notify.enable = true;
      bufferline = {
        enable = true;
        settings.options.diagnostics = "nvim_lsp";
      };
      fidget.enable = true;
      lualine = {
        enable = true;
        settings = {
          options.globalstatus = true;
          sections = {
            lualine_a = [ "mode" ];
            lualine_b = [ "diagnostics" ];
            lualine_c = [
              {
                __unkeyed-1 = "filename";
                path = 1;
              }
              "filesize"
            ];
            lualine_x = [
              "encoding"
              "fileformat"
              "filetype"
            ];
            lualine_y = [ "progress" ];
            lualine_z = [ "location" ];
          };
        };
      };
      which-key = {
        enable = true;
        settings = {
          preset = "modern";
          keys = {
            scrool_down = "<c_j>";
            scrool_up = "<c_k>";
          };
          spec = [
            {
              __unkeyed-1 = "<leader>b";
              group = " Buffer";
            }
            {
              __unkeyed-1 = "<leader>f";
              group = "󰍉 Find";
            }
            {
              __unkeyed-1 = "<leader>c";
              group = " Code";
            }
            {
              __unkeyed-1 = "<leader>g";
              group = " Git";
            }
            {
              __unkeyed-1 = "<leader>u";
              group = "󰍹 UI";
            }
            {
              __unkeyed-1 = "<leader>d";
              group = " Debug";
            }
          ];
        };
      };
      lsp-lines.enable = false;
      zen-mode = {
        enable = true;
        settings = {
          plugins.neovide.enabled = true;
          window.options = {
            signcolumn = "no";
            number = false;
            relativenumber = false;
            foldcolumn = "0";
          };
          on_open = ''
            function()
              local precognition = require("precognition")
              if precognition.toggle() then
                precognition.toggle()
              end
            end
          '';
          on_close = ''
            function()
              local precognition = require("precognition")
              if not precognition.toggle() then
                precognition.toggle()
              end
            end
          '';
        };
      };
    };
    autoCmd = [
      {
        callback.__raw = ''
          function()
            if vim.g.neovide then
              vim.o.guifont = "FiraCode Nerd Font"
              vim.g.neovide_cursor_animate_command_line = false
              vim.g.neovide_cursor_vfx_mode = "railgun"

              local change_scale_factor = function(delta)
                vim.g.neovide_scale_factor = vim.g.neovide_scale_factor * delta
              end
              vim.keymap.set("", "<C-=>", function()
                change_scale_factor(1.25)
              end)
              vim.keymap.set("", "<C-->", function()
                change_scale_factor(1 / 1.25)
              end)
            end
          end
        '';
        event = "UiEnter";
      }
    ];
    keymaps = [
      {
        key = "<leader>uz";
        action = "<cmd>ZenMode<cr>";
        options.desc = "󰠖 Zen Mode";
      }
    ];
  };
}

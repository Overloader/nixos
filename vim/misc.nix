{ ... }:
{
  programs.nixvim = {
    plugins = {
      auto-save = {
        enable = false;
        settings = {
          condition = ''
            function(buf)
              local fn = vim.fn
              local utils = require("auto-save.utils.data")

              if utils.not_in(fn.getbufvar(buf, "&filetype"), {'oil'}) then
                return true
              end
              return false
            end
          '';
          debounce_delay = 5000;
        };
      };
      direnv.enable = true;
      comment.enable = true;
      hardtime.enable = true;
      nvim-autopairs.enable = true;
      precognition = {
        enable = true;
        settings = {
          showBlankVirtLine = false;
          disabled_fts = [ "dashboard" ];
        };
      };
      rainbow-delimiters.enable = true;
      snacks = {
        enable = true;
        settings = {
          indent.enable = true;
        };
      };
      todo-comments.enable = true;
    };
    globals.dim = false;
    keymaps = [
      {
        key = "<leader>gl";
        action.__raw = ''
          function()
            require("snacks").lazygit.open()
          end
        '';
        options.desc = " LazyGit";
      }
      {
        key = "<F6>";
        action.__raw = ''
          function()
            require("snacks").terminal.toggle()
          end
        '';
        options.desc = " Terminal";
        mode = [
          "n"
          "t"
        ];
      }
      {
        key = "<leader>ud";
        action.__raw = ''
          function()
            local dim = require("snacks").dim
            if vim.g.dim then
              dim.disable()
            else
              dim.enable()
            end
            vim.g.dim = not vim.g.dim
          end
        '';
        options.desc = " Dim";
      }
    ];
  };
}

{ ... }:
{
  programs.nixvim = {
    plugins.telescope = {
      enable = true;
      extensions = {
        fzf-native.enable = true;
        undo.enable = true;
      };
    };
    keymaps = [
      {
        key = "<leader>f<space>";
        action = "<cmd>Telescope buffers<cr>";
        options.desc = "󰷊 Open Buffers";
      }
      {
        key = "<leader>f/";
        action = "<cmd>Telescope current_buffer_fuzzy_find<cr>";
        options.desc = "󰍉 Current File";
      }
      {
        key = "<leader>ff";
        action = "<cmd>Telescope find_files<cr>";
        options.desc = " Files";
      }
      {
        key = "<leader>fg";
        action = "<cmd>Telescope live_grep<cr>";
        options.desc = "󰱼 Grep across Files";
      }
      {
        key = "<leader>fh";
        action.__raw = ''
          function()
            require("telescope.builtin").find_files({ hidden = true })
          end
        '';
        options.desc = "󰘓 Hidden Files";
      }
      {
        key = "<leader>fu";
        action.__raw = ''
          function()
            telescope.extensions.undo.undo()
          end
        '';
        options.desc = " Undo Tree";
      }
      {
        key = "<leader>ft";
        action = "<cmd>TodoTelescope<cr>";
        options.desc = " Todos";
      }
    ];
  };
}

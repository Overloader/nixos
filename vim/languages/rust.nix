{ pkgs, ... }:
{
  imports = [ ./toml.nix ];
  programs.nixvim.plugins = {
    treesitter.grammarPackages = [
      pkgs.vimPlugins.nvim-treesitter.builtGrammars.rust
    ];
    rustaceanvim = {
      enable = true;
      rustAnalyzerPackage = null;
    };
    conform-nvim.settings.formatters_by_ft.rust = [ "rustfmt" ];
    lint.lintersByFt.rust = [ "clippy" ];
  };
}

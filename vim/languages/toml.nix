{ pkgs, ... }:
{
  programs.nixvim.plugins = {
    treesitter.grammarPackages = [
      pkgs.vimPlugins.nvim-treesitter.builtGrammars.toml
    ];
    lsp.servers.taplo = {
      enable = true;
      package = null;
    };
    conform-nvim.settings.formatters_by_ft.toml = [ "taplo" ];
  };
}

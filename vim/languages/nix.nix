{ pkgs, ... }:
{
  programs.nixvim = {
    plugins = {
      treesitter.grammarPackages = [
        pkgs.vimPlugins.nvim-treesitter.builtGrammars.nix
      ];
      lsp.servers.nil_ls.enable = true;
      conform-nvim.settings.formatters_by_ft.nix = [ "nixfmt" ];
    };
    files = {
      "ftplugin/nix.lua" = {
        localOpts = {
          expandtab = true;
          shiftwidth = 2;
          tabstop = 2;
        };
      };
    };
  };
}

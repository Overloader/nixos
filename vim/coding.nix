{ pkgs, ... }:
{
  imports = [ ./languages/default.nix ];
  programs.nixvim = {
    plugins = {
      blink-cmp = {
        enable = true;
        settings = {
          sources = {
            default = [
              "lsp"
              "path"
              "snippets"
              "buffer"
            ];
          };
          completion = {
            documentation = {
              auto_show = true;
              window = {
                winblend = 10;
                border = "double";
              };
            };
            list.selection = {
              preselect = false;
              auto_insert = false;
            };
            ghost_text.enabled = true;
            menu = {
              border = "double";
              # TODO: draw
              winblend = 10;
            };
          };
          signature = {
            enabled = true;
            window = {
              border = "double";
              winblend = 10;
            };
          };
          keymap = {
            preset = "enter";
            "<c-h>" = [ "scroll_documentation_up" ];
            "<c-l>" = [ "scroll_documentation_down" ];
            "<c-j>" = [ "select_next" ];
            "<c-k>" = [ "select_prev" ];
          };
        };
      };
      friendly-snippets.enable = true;
      lspsaga = {
        enable = true;
        lightbulb.enable = true;
        symbolInWinbar.enable = true;
      };
      treesitter = {
        enable = true;
        nixvimInjections = true;
        settings = {
          highlight.enable = true;
          indent.enable = true;
        };
      };
      treesitter-context.enable = true;
      lsp = {
        enable = true;
        inlayHints = true;
        keymaps = {
          extra = [
            {
              action.__raw = ''
                function()
                  require("telescope.builtin").diagnostics()
                end
              '';
              key = "<leader>fd";
              mode = "n";
              options = {
                buffer.__raw = "bufnr";
                desc = " Diagnostics";
              };
            }
            {
              action.__raw = ''
                function()
                  require("telescope.builtin").lsp_definitions()
                end
              '';
              key = "gd";
              mode = "n";
              options = {
                buffer.__raw = "bufnr";
                desc = "󰊕 Definition";
              };
            }
            {
              action.__raw = ''
                function()
                  require("telescope.builtin").lsp_implementations()
                end
              '';
              key = "gI";
              mode = "n";
              options = {
                buffer.__raw = "bufnr";
                desc = " Implementation";
              };
            }
            {
              action.__raw = ''
                function()
                  require("telescope.builtin").lsp_references()
                end
              '';
              key = "gr";
              mode = "n";
              options = {
                buffer.__raw = "bufnr";
                desc = " References";
              };
            }
            {
              action = "<cmd>Lspsaga code_action<cr>";
              key = "<leader>ca";
              mode = "n";
              options = {
                buffer.__raw = "bufnr";
                desc = " Code Action";
              };
            }
            {
              action = "<cmd>Lspsaga rename<cr>";
              key = "<leader>cr";
              mode = "n";
              options = {
                buffer.__raw = "bufnr";
                desc = " Rename";
              };
            }
          ];
          lspBuf = {
            gD = "declaration";
            K = "hover";
          };
        };
      };
      conform-nvim = {
        enable = true;
        settings = {
          format_on_save = {
            lsp_format = "fallback";
            timeout_ms = 500;
          };
          formatters_by_ft = {
            "_" = [ "trim_whitespace" ];
          };
          default_format_opts.lsp_format = "fallback";
        };
      };
      compiler.enable = true;
      lint = {
        enable = false;
        autoCmd = {
          callback.__raw = ''
            function()
              require("lint").try_lint()
            end
          '';
          event = "BufWritePost";
        };
      };
    };
    keymaps = [
      {
        key = "<leader>co";
        action = "<cmd>Lspsaga outline<CR>";
        options.desc = " Code outline";
      }
      {
        action = "<cmd>CompilerOpen<cr>";
        key = "<leader>cc";
        options.desc = " Compiler";
      }
    ];
    extraPackages = with pkgs; [ nixfmt-rfc-style ];
  };
}
